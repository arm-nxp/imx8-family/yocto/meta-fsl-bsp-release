# Copyright (C) 2013-2016 Freescale Semiconductor
# Copyright 2017-2018 NXP
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "Linux Kernel provided and supported by NXP, modified by congatec"
DESCRIPTION = "Linux Kernel provided and supported by NXP with focus on \
i.MX Family Reference Boards. It includes support for many IPs such as GPU, VPU and IPU. \
Support for congatec SX8M added."

require recipes-kernel/linux/linux-imx.inc

SRCBRANCH = "cgtimx8mm__imx_4.14.98_2.0.0_ga"
LOCALVERSION = "-${SRCBRANCH}"
KERNEL_SRC = "git://git.congatec.com/arm-nxp/imx8-family/kernel-imx8-family.git;protocol=https"
SRC_URI = "${KERNEL_SRC};branch=${SRCBRANCH}"
SRCREV = "fd8fddfad874a9aff97024752b677f6b6bd58573"

S = "${WORKDIR}/git"

DEPENDS += "lzop-native bc-native"

DEFAULT_PREFERENCE = "1"

addtask copy_defconfig after do_unpack before do_preconfigure
do_copy_defconfig () {
    install -d ${B}

    # copy sx8m defconfig
    mkdir -p ${B}
    cp ${S}/arch/arm64/configs/sx8m_defconfig ${B}/.config
    cp ${S}/arch/arm64/configs/sx8m_defconfig ${B}/../defconfig
}

COMPATIBLE_MACHINE = "(sx8m)"
