UBOOT_SRC ?= "git://git.congatec.com/arm-nxp/imx8-family/uboot-imx8-family.git;protocol=https"
SRCBRANCH = "cgtimx8mm__imx_v2018.03_4.14.98_2.0.0_ga"
SRC_URI = "${UBOOT_SRC};branch=${SRCBRANCH}"
SRCREV = "039b578bd0787520dbeb0eb34fa9bf2a0728f721"
