# Copyright 2017-2018 NXP

IMX_MKIMAGE_SRC ?= "git://git.congatec.com/arm-nxp/imx8-family/misc/mkimage-imx8-family.git;protocol=https"
SRCBRANCH = "cgtimx8mm__imx_4.14.98_2.0.0_ga"
SRC_URI = "${IMX_MKIMAGE_SRC};branch=${SRCBRANCH}"
SRCREV = "acab9050f5404aadfb6944056aa863840fc2ac39"

DEPENDS = "zlib-native openssl-native"
